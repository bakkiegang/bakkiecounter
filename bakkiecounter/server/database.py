from peewee import *

db = PostgresqlDatabase(None)


def connect(host, port, name, user, pw, create_tables=False) -> None:
    """Initializes the database session and connects to the database

    :param host: Database host
    :param port: Database port
    :param name: Database name
    :param user: Database user
    :param pw: Database password of user
    :param create_tables: Creates database tables [default: False]
    """
    db.init(name, host=host, port=port, user=user, password=pw)
    db.connect()

    if create_tables:
        db.create_tables([User, Auth, Session, Entry])


class BaseModel(Model):
    """Base model for all following models"""

    id = AutoField()

    class Meta:
        database = db


class User(BaseModel):
    mail = CharField(unique=True, max_length=255)
    name = CharField(max_length=255, unique=True)
    active = BooleanField(null=False, default=True)


class Auth(BaseModel):
    user_id = ForeignKeyField(User, backref="auth", unique=True)
    pw_hash = BlobField()


class Session(BaseModel):
    user_id = ForeignKeyField(User, backref="session")
    token = CharField(max_length=255)


class Entry(BaseModel):
    user_id = ForeignKeyField(User, backref="entries")
    ts = TimestampField(utc=True)