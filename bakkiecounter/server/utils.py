import hashlib

import cherrypy

# TODO: Consider security.salt from config
def hash(val):
    """Hashes a string value

    :param val: String to hash
    :return: hashed string
    """
    # TODO: Passlib will consider als the salt.
    return hashlib.sha512(val.encode("utf-8")).digest()


def verify_user(user_id):
    if cherrypy.session.get("uid") != user_id:
        raise cherrypy.HTTPError(401)
