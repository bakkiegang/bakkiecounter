import cherrypy

class Server(object):
    def __init__(self):
        pass

    @cherrypy.expose
    def counter(self, user_id=0):
        """ Do nothing but redirect to front page;
        Required by Vue router history
        """
        raise cherrypy.HTTPRedirect('/')

    @cherrypy.expose
    def login(self):
        """ Do nothing but redirect to front page;
        Required by Vue router history
        """
        raise cherrypy.HTTPRedirect('/')

