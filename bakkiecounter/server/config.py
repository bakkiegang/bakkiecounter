import toml

from toml import TomlDecodeError

PATH = "config.toml"

DEFAULT = {
    "database": {
        "host": "localhost",
        "port": 8080,
        "name": "bakkiecounter-default",
        "user": "bakkiecounter",
        "password": "secret",
        "migration": {"script-dir": "src/bakkiecounter/database"},
    },
    "log": {
        "access": "/var/log/bakkiecounter/access.log",
        "error": "/var/log/bakkiecounter/error.log",
        "debug": False,
    },
    "web": {
        "host": "localhost",
        "port": 8080,
        "static-dir": "public",
    },
}


def init(f: str):
    """Initializes a configuration file with the default parameters

    :param f: Path of new configuration file
    """
    with open(f, "w") as _f:
        toml.dump(DEFAULT, _f)
