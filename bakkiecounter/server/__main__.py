#!/usr/bin/python3


import argparse
import cherrypy
import os
import yacf

from bakkiecounter.server import config, database, router
from bakkiecounter.server.api import auth, entry, user, Api

version_info = (0, 0, 0)
version = ".".join(str(c) for c in version_info)


def _load_config(path: str) -> yacf.Configuration:
    # Init config file if required
    if not os.path.isfile(path):
        config.init(path)
        cherrypy.log(f"Initialized configuration file {path}.")
        cherrypy.log("Please configure and restart the server.")
        _shutdown(0)

    # Load configuration
    cfg = yacf.Configuration(config.DEFAULT, path)
    try:
        cfg.load()
    except Exception as e:
        cherrypy.log.error(
            f"Failed to load configuration. Check configuration file {path}"
        )
        _shutdown(1)

    cherrypy.log(f"Successfully loaded configuration from {path}.")

    return cfg


def _parse_args():
    parser = argparse.ArgumentParser(
        description="Startup an instance of a bakkiecounter service"
    )

    parser.add_argument(
        "-c",
        "--config",
        type=str,
        default=config.PATH,
        help="Path to a configuration file",
    )

    parser.add_argument(
        "-m",
        "--migrate",
        action="store_true",
        default=False,
        help="Migrates or initializes the database.",
    )

    parser.add_argument(
        "--version",
        action="version",
        version="%(prog)s {}".format(version),
        help="show the version number and exit",
    )

    return parser.parse_args()


def _prepare_logging(access_file: str, error_file: str, debug: bool):
    """Ensures logging will work flawlessly, e.g. creates logging directories
    and files and configures cherrypy to run in debug mode, if configured.

    :param access_file: Path to the access log file
    :param error_file: Path to the error log file
    :param debug: Debug flag, to ensure cherrypy will log in debug mode
    """
    # Create log directories
    os.makedirs(os.path.dirname(error_file), exist_ok=True)
    os.makedirs(os.path.dirname(access_file), exist_ok=True)

    if not os.path.isfile(error_file):
        with open(error_file, "w+") as _f:
            _f.write("")

    if not os.path.isfile(access_file):
        with open(access_file, "w+") as _f:
            _f.write("")
    # Setup logging
    cherrypy.log.access_file = access_file
    cherrypy.log.error_file = error_file

    cherrypy.log("Access Log:       {access_file}")
    cherrypy.log("Error Log:        {error_file}")
    if debug:
        cherrypy.log("Debug mode activated; Log will be printed to stdout")


def _shutdown(code=0):
    cherrypy.log("Shutdown server")
    exit(code)


def main(*args, **kwargs):
    # TODO: Fix cherrypy.log calls (ensure they are not printed to access log, but to error log.)
    args = _parse_args()
    cherrypy.log.screen = True

    cfg = _load_config(args.config)

    # Read logger information
    access_f = cfg.get("log.access")
    error_f = cfg.get("log.error")
    _debug = cfg.get("log.debug")
    cherrypy.log.screen = _debug

    _prepare_logging(access_f, error_f, _debug)

    # Begin with server startup sequence
    cherrypy.log("Starting server...")

    # Connect to database
    cherrypy.log(
        f"Connect to database {cfg.get('database.name')} ({cfg.get('database.host')}:{cfg.get('database.port')})"
    )
    if args.migrate:
        cherrypy.log("Database will be initialized after connecting to it.")
    database.connect(
        cfg.get("database.host"),
        cfg.get("database.port"),
        cfg.get("database.name"),
        cfg.get("database.user"),
        cfg.get("database.password"),
        args.migrate,
    )
    cherrypy.log("Successfully connected to database.")

    # CherryPy configuration
    conf = {
        "/": {
            "tools.sessions.on": True,
            "tools.staticdir.on": True,
            "tools.staticdir.dir": os.path.abspath(
                os.getcwd() + "/" + cfg.get("web.static-dir")
            ),
            "tools.staticdir.index": "index.html",
        },
    }

    # Mounting /api
    cherrypy.log("Mounting /api")
    cherrypy.tree.mount(router.Server(), "/", conf)
    cherrypy.tree.mount(
        Api(),
        "/api",
        {
            "/": {
                "tools.response_headers.on": True,
                "tools.response_headers.headers": [("Content-Type", "text/plain")],
            }
        },
    )
    cherrypy.log("Successfully mounted /api")

    # Mounting /api/users
    cherrypy.log("Mounting /api/users")
    cherrypy.tree.mount(
        user.UserApi(),
        "/api/users",
        {
            "/": {
                "request.dispatch": cherrypy.dispatch.MethodDispatcher(),
                "tools.sessions.on": True,
            }
        },
    )
    cherrypy.log("Successfully mounted /api/users")

    # Mounting /api/auth
    cherrypy.log("Mounting /api/auth")
    cherrypy.tree.mount(
        auth.AuthApi(),
        "/api/auth",
        {
            "/": {
                # 'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                "tools.sessions.on": True
            }
        },
    )
    cherrypy.log("Successfully mounted /api/auth")

    # Configure socket
    cherrypy.server.socket_host = cfg.get("web.host")
    cherrypy.server.socket_port = cfg.get("web.port")

    # Fire up cherrypy
    cherrypy.engine.start()
    cherrypy.log("Started server successfully.")

    # TODO: load cached sessions of database
    cherrypy.engine.block()


if __name__ == "__main__":
    main()
