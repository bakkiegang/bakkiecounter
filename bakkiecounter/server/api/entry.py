from datetime import datetime

import cherrypy

from peewee import DoesNotExist
from playhouse.shortcuts import model_to_dict

from bakkiecounter.server import database as db
from bakkiecounter.server import utils


@cherrypy.expose
@cherrypy.popargs("entry_id")
@cherrypy.tools.json_out()
class EntryApi(object):
    def __init__(self):
        pass

    def GET(self, user_id, entry_id=-1):
        user_id = int(user_id)
        entry_id = int(entry_id)

        utils.verify_user(user_id)

        if entry_id < 0:
            entries = db.Entry.select().where(db.Entry.user_id == user_id)
        else:
            entries = db.Entry.select().where(
                db.Entry.user_id == user_id & db.Entry.id == entry_id
            )

        for i in range(len(entries)):
            entries[i].ts = format(entries[i].ts)
        return [model_to_dict(e) for e in entries]

    def POST(self, user_id, entry_id=-1):
        user_id = int(user_id)
        utils.verify_user(user_id)

        entry = db.Entry.create(user_id=user_id, ts=datetime.utcnow())
        entry.save()

        entry.ts = format(entry.ts)
        return model_to_dict(entry, recurse=False)

    def DELETE(self, user_id, entry_id=-1):
        user_id = int(user_id)
        entry_id = int(entry_id)

        utils.verify_user(user_id)

        try:
            entry = db.Entry.get_by_id(entry_id)

            if not entry.user_id == user_id:
                raise cherrypy.HTTPError(
                    403, "Forbidden: Can not access entries of other users."
                )

            entry.delete_instance()
            entry.ts = format(entry.ts)
            return entry
        except DoesNotExist:
            raise cherrypy.HTTPError(404, f"Invalid entry id ({entry_id})")
