import cherrypy
import json

from bakkiecounter.server import database as db
from bakkiecounter.server import utils

from peewee import DoesNotExist
from playhouse.shortcuts import model_to_dict


@cherrypy.expose
class AuthApi(object):
    def __init__(self):
        pass

    @cherrypy.expose
    @cherrypy.tools.json_in()
    @cherrypy.tools.json_out()
    def login(self, from_page="/"):
        """User login

        :param from_page: possibly a page redirection
        :return: Dictionary with UID and a possibly non-empty error string
        """

        # Limit access on POST requests
        if cherrypy.request.method != "POST":
            raise cherrypy.HTTPError(401)

        req = cherrypy.request.json

        if req["name"] is None or req["pw"] is None:
            raise cherrypy.HTTPError(400, "Illegal username or password.")

        # Check authentication
        user = authenticate(req["name"], req["pw"])

        # Create a session
        sid = cherrypy.session.generate_id()
        cherrypy.session["uid"] = user.id
        cherrypy.session["sid"] = sid

        # login logic
        cherrypy.request.login = user.id
        self._on_login(user.id)

        return model_to_dict(user, recurse=False)

    @cherrypy.expose
    def logout(self, from_page="/"):
        """User logout

        :param from_page: possibly a page redirection
        :return:
        """

        # Limit access on POST requests
        if cherrypy.request.method != "POST":
            raise cherrypy.HTTPError(401)

        uid = cherrypy.session.get("uid")

        cherrypy.session["uid"] = None
        cherrypy.session["sid"] = None

        if uid:
            cherrypy.request.login = None
            self._on_logout(uid)

        # TODO: Delete cookie deletion to logout

        raise cherrypy.HTTPRedirect(from_page)

    def _on_login(self, uid):
        """Called on successful login"""
        # TODO: Store session on login
        pass

    def _on_logout(self, uid):
        """Called on successful logout"""
        # TODO: Delete session on logout
        pass


def authenticate(username: str, pw: str) -> db.User:
    """Checks credentials of a user and authenticates the user.
    :param username: User name
    :param pw: password
    :return: Returns the user record of the database.
    """
    pw_hash = utils.hash(pw)
    pw = None

    _401 = cherrypy.HTTPError(401, "Invalid combination of username and password.")
    _403 = cherrypy.HTTPError(403, "User account is blocked.")

    try:
        user = db.User.get(db.User.name == username)

        if not user.active:
            raise _403

        auth = db.Auth.get(db.Auth.user_id == user.id)
    except DoesNotExist:
        raise _401

    if pw_hash != bytes(auth.pw_hash):
        raise _401

    return user


def update_auth_details(user_id: int, pw: str):
    """Updates authentication details of a user
    :param user_id: User id
    :param pw: The user's password
    """
    pw_hash = utils.hash(password)
    pw = None

    try:
        auth = db.Auth.get_or_create(db.Auth.user_id == user_id)
        auth.pw_hash = bytes(pw_hash)
        auth.save()
    except Exception as e:
        print(e)
