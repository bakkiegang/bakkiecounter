import json

from datetime import datetime

import cherrypy

from peewee import DoesNotExist
from playhouse.shortcuts import model_to_dict

from bakkiecounter.server import database as db
from bakkiecounter.server import utils
from bakkiecounter.server.api import entry


@cherrypy.expose
@cherrypy.popargs("user_id")
@cherrypy.tools.json_out()
class UserApi(object):
    def __init__(self):
        self.entries = entry.EntryApi()

    def GET(self, user_id=-1):
        user_id = int(user_id)
        utils.verify_user(user_id)

        try:
            user = db.User.get_by_id(user_id)
            return model_to_dict(user, recurse=False)
        except DoesNotExist:
            raise cherrypy.HTTPError(400, "Unknown user")

    @cherrypy.tools.json_in()
    def POST(self):
        req = cherrypy.request.json

        mail = req.get("mail", "")
        name = req.get("name")
        pw = req.get("pw")

        # validate input
        if not name or not pw:
            raise cherrypy.HTTPError(
                400, "Requires both parameters of username and password to be set."
            )

        # Hash password
        pw_hash = utils.hash(req["pw"])
        req["pw"] = ""

        # Ensure no user with same name exists
        if len(db.User.select().where(db.User.name == name)):
            raise cherrypy.HTTPError(409, "Username already in use")

        # Insert user
        user = db.User.create(
            mail=mail,
            name=name,
            active=True,
        )
        user.save()

        # Insert auth
        user_auth = db.Auth.create(
            user_id=user.id,
            pw_hash=pw_hash,
        )
        user_auth.save()

        return model_to_dict(user, recurse=False)
