import cherrypy


class Api(object):
    @cherrypy.expose
    def index(self):
        return """
================================================================================
  Bakkiecounter API
================================================================================

Welcome curious nerd! Here you can find some little documentation about the API.
Feel free to develop your own clients based on this service. Please do not
hesitate to contanct one of the developers. We appreciate bug reports and would
love to receive feature requests.


================================================================================
  API Methods
================================================================================

POST    bakkiecounter.com/api/auth/login
        request : {
            name : <User name>,
            pw   : <Password>,
        }

    User login


POST    bakkiecounter.com/api/auth/logout

    You guessed it: user logout
    The user will be redirected to the front page.


GET     bakkiecounter.com/api/users/

    Permitted accounts can read the list of all users


POST    bakkiecounter.com/api/users/
        request : {
            name : <User name>,
            mail : <Mail address> [Optional],
            pw   : <Password>
        }

    Create a new User account for the API. Required are the fields 'name' and 'pw'.
    The mail address is an optional field.


GET     bakkiecounter.com/api/users/<user_id>/

    Query the data of a specific user. Non-privileged users can just query their
    own user data. Priviliged users can access the profile of each user.


GET     bakkiecounter.com/api/users/<user_id>/entries

    Query a list of the user's counter records. Non-privileged users can just
    query their own user data.


POST    bakkiecounter.com/api/users/<user_id>/entries

    Create a new counter record for a specific user. As always, non-privileged
    users can not alter any data of user accounts others than themselves.


GET     bakkiecounter.com/api/users/<user_id>/entries/count

    Query the number entries that a user has. In the future, optional filter
    parameters can be used to filter on given timeranges.


GET     bakkiecounter.com/api/users/<user_id>/entries/<entry_id>

    Query a specific counter record. Non-priviliged users can only query their
    own entries.


DELETE  bakkiecounter.com/api/users/<user_id>/entries/<entry_id>

    Delete a specific entry from the user's counter records. Non-privileged
    users ... you know it.


================================================================================
  Thank you!
================================================================================

Lastly, thank you for your interest and enjoy the API!

"""