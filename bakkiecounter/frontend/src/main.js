import Vue from 'vue'
import App from './App.vue'
import router from './router'

// Axios
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios);


// Md
import {MdMenu, MdList, MdAvatar, MdContent, MdSnackbar, MdApp, MdToolbar, MdField, MdEmptyState, MdDialog, MdCard, MdButton, MdIcon} from 'vue-material/dist/components'
import 'vue-material/dist/vue-material.min.css'
import 'vue-material/dist/theme/default-dark.css'

Vue.use(MdMenu);
Vue.use(MdList);
Vue.use(MdAvatar);
Vue.use(MdContent);
Vue.use(MdSnackbar);
Vue.use(MdApp);
Vue.use(MdToolbar);
Vue.use(MdField);
Vue.use(MdEmptyState);
Vue.use(MdDialog);
Vue.use(MdCard);
Vue.use(MdButton);
Vue.use(MdIcon);


// Vuelidate
import Vuelidate from 'vuelidate'
Vue.use(Vuelidate)


Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App),
}).$mount('#app');
