import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login.vue'
import Counter from '../components/Counter.vue'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/login',
    name: 'login',
    component: Login,
  },
  {
    path: '/counter/:userId',
    name: 'counter',
    component: Counter,
    props: true,
  },
  {
    path: '*',
      redirect: '/login'
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

export default router
