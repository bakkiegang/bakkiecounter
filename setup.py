from setuptools import setup, find_namespace_packages

from src.bakkiecounter import server
from bakkiecounter.server import utils

setuptools.setup(
    name='bakkiecounter',
    version=server.version,
    description='',
    url='https://gitlab.com/bakkiegang/bakkiecounter/',
    packages=['bakkiecounter'],
    entry_points={
        # TODO: Add script to initialize database & configuration file
        'console_scripts': ['bakkieserver = bakkiecounter:server:main_func'],
    },
)

# TODO: Add to a script to initialize database
#if __name__ == '__main__':
#    utils.setup_database()
