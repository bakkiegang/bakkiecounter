# NOTICE: This repository has moved to Codeberg

[https://codeberg.org/bakkie2go](https://codeberg.org/bakkie2go)

## Bakkiecounter

This repository hosts the code of a simple hobby project to track one's coffee consumption.
If you like this idea, then either clone the repository and host it yourself, or use our public instance on [bakkiecounter.com](https://bakkiecounter.com). 

Software stack:

* [Vue.js](https://vuejs.org/) as the frontend
* [Cherrypy](https://cherrypy.org/) as the backend
* [PostgreSQL](https://www.postgresql.org/) as the database system

## Front-end

```
TODO: Quickly describe the technical features of the front-end
```

## Back-end

The back-end uses [CherryPy](https://cherrypy.org/).

To start the back-end, simply execute the server.
At this version, it is important to execute the server from the root directory,
e.g:

```
	python src/server.py
	# or alternatively
	src/server.py
```

## Database

The only supported database system is [PostgreSQL](https://www.postgresql.org/).
We do not plan to add support for other database systems.

To operate, a new database needs to be created.
A new user requires permissions to create, update and delete tables as well as CRUD permissions on all non-system tables, that the user creates.

## API

The following excerpt is copied from [bakkiecounter.com/api](https://bakkiecounter.com/api/):

```
POST    bakkiecounter.com/api/auth/login
        request : {
            name : <User name>,
            pw   : <Password>
        }
        
    User login


POST    bakkiecounter.com/api/auth/logout

    You guessed it: user logout


GET     bakkiecounter.com/api/users/

    Permitted accounts can read the list of users


POST    bakkiecounter.com/api/users/
        request : {
            name : <User name>,
            mail : <Mail address>,
            pw   : <Password> 
        }

    Create a new User account for the API


GET     bakkiecounter.com/api/users/<user_id>/

    Query the data of a specific user. Non-privileged users can just query their
    own user data.


GET     bakkiecounter.com/api/users/<user_id>/entries

    Query a list of the user's counter records. Non-privileged users can just 
    query their own user data.


POST    bakkiecounter.com/api/users/<user_id>/entries

    Create a new counter record for a specific user. As always, non-privileged
    users can not alter any data of user accounts others than themselves.


GET     bakkiecounter.com/api/users/<user_id>/entries/<entry_id>

    Query a specific counter record, when having permission.


DELETE  bakkiecounter.com/api/users/<user_id>/entries/<entry_id>

    Delete a specific entry from the user's counter records. Non-privileged
    users ... you know it.
```

## Config

When running the setup script or running the server the first time, a configuration file is generated with default values.
The configuration is in [TOML](https://toml.io/en/) format and consists of three parts:

**Database:**

```
   1   │ [database]
   2   │
   3   │ host = "localhost"         # The hostname of the database server
   4   │ port = 8080                # Postgres port
   5   │ name = "bakkiecounter"     # Name of the Database
   6   │ user = "postgres"          # Username of the database user
   7   │ password = ""              # Password of database user
```

Ensure, that the database server is secured properly!

**Logging:**

```
   1   │ [log]
   2   │ access_log = "/var/log/access.log"     # Location of the access log
   3   │ error_log = "/var/log/error.log"       # Location of the error log
   4   │ debug = false                          # Debug mode
```

The debug mode can be enabled. It will ensure, that the stack trace will also be logged.
Additionally, will the entire log also be printed to the STDOUT.

**Web Service:**

```
   1   │ [web]
   2   │ host = "localhost"         # Hostname or IP address to listen on
   3   │ port = 8080                # Service port
   4   │ static_dir = "public"      # Directory of the hosted front-end
```

The front-end we provide is a simple vue-based front-end.
However, any front-end can be added hosted, therefore, one can specify the static directory of the publicly accessible files.

## TODO

There are still plenty things to do to increase usability, user experience, additional features or documentation.
We try to keep track of it here in the README.

**API:**

* ~~/auth/logout gives a 401~~

**Backend:**

* ~~Add a proper default config file~~
* ~~Enable specific config as command line argument~~
* Specify common configuration parameters as args

**Database:**

* Persistent sessions (not just cached)

**Developer Experience:**

* Setup script
* Deployment script
* Deployment pipeline and/or auto-update on hosted services
* Systemd service files
* Pip setuptools (requirements cherrypy, pscopg2, toml)

**Frontend:**

* Vue router history redirection

**User Experience:**

* ~~Cookie & Privacy Policy~~
* ~~Show user name & profile information~~
* Password changes
* Account deletion
* When logged in, logout & login with other account fails (500)
* ~~Adding a user account that exists gives unclear error message~~
* ~~Generic error messages should be more helpful to user~~

