#!/bin/bash

cd src/bakkiecounter/frontend

npm run build

cd ../../..

rm -rfv public

cp -rv src/bakkiecounter/frontend/dist public

sudo chmod 755 -Rv public
